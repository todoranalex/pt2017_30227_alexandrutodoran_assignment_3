package bll;

import DAO.CustomerDAO;
import model.Customer;
import model.TableModel;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Alex on 4/11/2017.
 */
public class CustomerBLL {

    private CustomerDAO customerDAO;


    public CustomerBLL(){
        customerDAO = new CustomerDAO();
    }


    public Customer findClientById(int id) {
        Customer customer = customerDAO.findById(id);
        if (customer == null) throw new NoSuchElementException("The client with id =" + id + " was not found!");
        return customer;
    }
    public List<Customer> findAllCustomers(){
        List<Customer> customers = customerDAO.findAll();
        if (customers == null) {
            throw new NoSuchElementException("No products in the table");
        }

        return customers;
    }

    public String[] getAllColumns(){
        String[] columns = customerDAO.getColumns();
        if( columns == null){
            throw new NoSuchElementException("No columns in the table");
        }
        return columns;
    }

    public String[][] getAllData(List<Customer> customer){
        String[][] data = customerDAO.getData(customer);
        if( data == null){
            throw new NoSuchElementException("No data in the table");
        }
        return data;

    }
    public Customer insert(List<Customer>customers){

        Customer toInsert = customerDAO.insert(customers);
        if( toInsert == null){
            throw new NoSuchElementException("Couldnt insert");
        }
        return toInsert;
    }

    public Customer update(Customer customer, String setWhat, String setWhere, int val1, int val2){

        Customer updated = customerDAO.update(customer,setWhat,setWhere,val1,val2);
        if( updated == null){
            throw new NoSuchElementException("Couldnt update ");
        }
        return updated;

    }

    public void delete(Customer customer, String field, int value){
        customerDAO.delete(customer,field,value);
    }

    public TableModel createTable(List<Customer> customers){
        TableModel table = customerDAO.createTable(customers);
        if( table == null){
            throw new NoSuchElementException("No table ");
        }
        return table;
    }
}
