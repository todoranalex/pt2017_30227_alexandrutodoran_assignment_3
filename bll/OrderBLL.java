package bll;

import DAO.OrderDAO;
import model.Orders;
import model.TableModel;

import javax.swing.*;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Alex on 4/19/2017.
 */
public class OrderBLL {


    /**
     * Created by Alex on 4/11/2017.
     */


    private OrderDAO orderDAO;


    public OrderBLL() {
        orderDAO = new OrderDAO();
    }


    public Orders findOrderById(int id) {

        Orders order = orderDAO.findById(id);
        if (order == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return order;
    }

    public List<Orders> findAllOrders() {
        List<Orders> orders = orderDAO.findAll();
        if (orders == null) {
            throw new NoSuchElementException("No products in the table");
        }

        return orders;
    }

    public String[] getAllColumns() {
        String[] columns = orderDAO.getColumns();
        if (columns == null) {
            throw new NoSuchElementException("No columns in the table");
        }
        return columns;
    }

    public String[][] getAllData(List<Orders> orders) {
        String[][] data = orderDAO.getData(orders);
        if (data == null) {
            throw new NoSuchElementException("No data in the table");
        }
        return data;

    }

    public Orders insert(List<Orders> orders) {

       Orders toInsert = orderDAO.insert(orders);
        if (toInsert == null) {
            throw new NoSuchElementException("Couldnt insert");
        }
        return toInsert;
    }

    public Orders update(Orders order, String setWhat, String setWhere, int val1, int val2) {

       Orders updated = orderDAO.update(order, setWhat, setWhere, val1, val2);
        if (updated == null) {
            throw new NoSuchElementException("Couldnt update ");
        }
        return updated;

    }

    public void delete(Orders order, String field, int value) {
        orderDAO.delete(order, field, value);
    }

    public TableModel createTable(List<Orders> orders) {
        TableModel table = orderDAO.createTable(orders);
        if (table == null) {
            throw new NoSuchElementException("No table ");
        }
        return table;
    }
}

