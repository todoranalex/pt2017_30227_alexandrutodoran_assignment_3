package bll;

import DAO.OrderItemDAO;
import model.OrderItem;
import model.TableModel;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Alex on 4/19/2017.
 */
public class OrderItemBLL {


    /**
     * Created by Alex on 4/11/2017.
     */


    private OrderItemDAO orderItemDAO;


    public OrderItemBLL() {
        orderItemDAO = new OrderItemDAO();
    }


    public List<OrderItem> findAllOrderItems() {
        List<OrderItem> orderItems = orderItemDAO.findAll();
        if (orderItems == null) {
            throw new NoSuchElementException("No products in the table");
        }

        return orderItems;
    }

    public String[] getAllColumns() {
        String[] columns = orderItemDAO.getColumns();
        if (columns == null) {
            throw new NoSuchElementException("No columns in the table");
        }
        return columns;
    }

    public String[][] getAllData(List<OrderItem> orders) {
        String[][] data = orderItemDAO.getData(orders);
        if (data == null) {
            throw new NoSuchElementException("No data in the table");
        }
        return data;

    }

    public OrderItem insert(List<OrderItem> orders) {

        OrderItem toInsert = orderItemDAO.insert(orders);
        if (toInsert == null) {
            throw new NoSuchElementException("Couldnt insert");
        }
        return toInsert;
    }

    public OrderItem update(OrderItem order, String setWhat, String setWhere, int val1, int val2) {

        OrderItem updated = orderItemDAO.update(order, setWhat, setWhere, val1, val2);
        if (updated == null) {
            throw new NoSuchElementException("Couldnt update ");
        }
        return updated;

    }

    public void delete(OrderItem order, String field, int value) {
        orderItemDAO.delete(order, field, value);
    }

    public TableModel createTable(List<OrderItem> orders) {
        TableModel table = orderItemDAO.createTable(orders);
        if (table == null) {
            throw new NoSuchElementException("No table ");
        }
        return table;
    }
}

