package bll;

import DAO.ProductDAO;
import model.Product;
import model.TableModel;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Alex on 4/11/2017.
 */
public class ProductBLL {

    private ProductDAO productDAO;

    public ProductBLL(){
        productDAO = new ProductDAO();
    }


    public Product findProductById(int id) {
        Product pr = productDAO.findById(id);
        if (pr == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return pr;
    }

    public List<Product> findAllProducts(){
        List<Product> products = productDAO.findAll();
        if (products == null) {
            throw new NoSuchElementException("No products in the table");
        }

        return products;
    }

    public String[] getAllColumns(){
        String[] columns = productDAO.getColumns();
        if( columns == null){
            throw new NoSuchElementException("No columns in the table");
        }
        return columns;
    }

    public String[][] getAllData(List<Product> products){
        String[][] data = productDAO.getData(products);
        if( data == null){
            throw new NoSuchElementException("No data in the table");
        }
        return data;

    }

    public Product insert(List<Product>list){

        Product toInsert = productDAO.insert(list);
        if( toInsert == null){
            throw new NoSuchElementException("Couldnt insert");
        }
        return toInsert;

    }

    public Product update(Product product, String setWhat, String setWhere, int val1, int val2){

        Product updated = productDAO.update(product,setWhat,setWhere,val1,val2);
        if( updated == null){
            throw new NoSuchElementException("Couldnt update ");
        }
        return updated;

    }

    public void delete(Product product, String field, int value){

         productDAO.delete(product,field,value);

    }



    public TableModel createTable(List<Product> products){
        TableModel table = productDAO.createTable(products);
        if( table == null){
            throw new NoSuchElementException("No table ");
        }
        return table;
    }


}
