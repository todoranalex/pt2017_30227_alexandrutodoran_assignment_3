package model;

import java.util.NoSuchElementException;

/**
 * Created by Alex on 4/11/2017.
 */
public class Customer {

    private int id;
    private String name;



    public Customer(){};

    public Customer(int id, String name){

        this.id = id;
        this.name = name;
        //this.order = 0;
       // this.product = 0;


    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName( String name){
        this.name = name;
    }

    public String toString(){
        return "model.Customer [id = " + id +", name = " + name +" ]";

    }

}

