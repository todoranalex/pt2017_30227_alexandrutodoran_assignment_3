package model;

/**
 * Created by Alex on 4/19/2017.
 */
public class OrderItem {

    private int id;
    private int order_id;
    private int product_id;
    private int cantitate;

    public OrderItem(){};

    public OrderItem(int id, int order_id,int product_id,int cantitate){
        this.id = id;
        this.order_id = order_id;
        this.product_id = product_id;
        this.cantitate = cantitate;
    }


    public int getId() {
        return id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
}
