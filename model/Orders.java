package model;

/**
 * Created by Alex on 4/11/2017.
 */
public class Orders {

    private int id;
    private int customer_id;


    public Orders(){};

    public Orders(int id, int customer_id){
        this.id = id;
        this.customer_id = customer_id;
    }

    public int getId(){
        return id;
    }

    public int getCustomer_id(){
        return customer_id;
    }

    public void setId(int id){
        this.id = id;
    }
    public void setCustomer_id(int customer_id){
        this.customer_id = customer_id;
    }


    public String toString(){

        return "Order [id = " + id +", customer_id = " + customer_id + "  ]";
    }
}
