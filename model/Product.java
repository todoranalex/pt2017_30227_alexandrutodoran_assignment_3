package model;

/**
 * Created by Alex on 4/11/2017.
 */
public class Product {


    private int id;
    private String name;
    private int stoc;
    private int pret;


    public Product(){};


    public Product(int id,String name,int stoc,int pret){
        super();
        this.id = id;
        this.name = name;
        this.stoc = stoc;
        this.pret = pret;

    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }
    public int getStoc(){
        return stoc;
    }

    public int getPret(){
        return pret;
    }

    public void setId( int id){
        this.id  = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setStoc(int stoc){
        this.stoc = stoc;
    }

    public void setPret(int price){
        this.pret = price;
    }

    public String toString(){

        return "model.Product [id = " + id +", name = " + name +", stoc = " + stoc + "  ]";
    }

}
