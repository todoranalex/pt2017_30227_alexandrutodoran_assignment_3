package model;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import static jdk.nashorn.internal.runtime.regexp.joni.Config.DEBUG;

/**
 * Created by Alex on 4/11/2017.
 */
public  class TableModel extends AbstractTableModel {

    private String[][] data;
    private String[] columns;

    public TableModel(String[][]data,String[]columns){

        this.data = data;
        this.columns = columns;
    }

    public int getRowCount(){
        return data.length;
    }

    public String getValueAt(int a,int b){
        return data[a][b];
    }
    public int getColumnCount() {
        return columns.length;
    }
    public String getColumnName(int col) {
        return columns[col];
    }

    public boolean isCellEditable(int row, int col) {
        if (col < 2) {
            return false;
        } else {
            return true;
        }
    }
    public void setValueAt(Object value, int row, int col) {
        if (DEBUG) {
            System.out.println("Setting value at " + row + "," + col
                    + " to " + value + " (an instance of "
                    + value.getClass() + ")");
        }

        data[row][col] = value.toString();
        fireTableCellUpdated(row, col);

    }





}
