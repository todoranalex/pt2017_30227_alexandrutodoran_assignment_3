package presentation;

import bll.CustomerBLL;
import bll.OrderBLL;
import bll.OrderItemBLL;
import bll.ProductBLL;
import model.*;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 4/11/2017.
 */
    public class Controller implements ActionListener {

    private View view;
    private View view1;
    private View view2;
    private View view3;
    private View view4;
    private TableModel table1;
    private TableModel table2;
    private TableModel table3;
    private TableModel table4;
    private JTable jtable1;
    private JTable jtable2;
    private JTable jtable3;
    private JTable jtable4;

    private String getCustomerData;
    private String getProductData;
    private int textDataStoc ;
    private static int nrOrder = 2;
    private static int nrOrderItem = 2;

    public Controller() {

        view = new View();
        view.attachButton1(this);
        view.attachButton2(new showCustomers());
        view.attachButton3(new addProduct());
        view.attachButton4(new addCustomer());
        view.attachButton5(new addOrder());
        view.attachButton6(new showOrders());
        view.attachButton7(new getData());
        view.attachButton8(new deleteCustomer());
        view.attachButton9(new deleteProduct());
        view.attachButton10(new updateCustomer());
        view.attachButton11(new updateProduct());
        view.attachButton14(new showOrderItems());

    }

    public void actionPerformed(ActionEvent e) {

        ProductBLL productBLL = new ProductBLL();
        List<Product> products = productBLL.findAllProducts();

        table1 = productBLL.createTable(products);
        jtable1 = new JTable(table1);
        view1 = new View(jtable1, 1);

        view1.attachButton12(new getProdId());
        view1.attachButton13(new getProdQ());

    }

    class showCustomers implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            CustomerBLL customerBLL = new CustomerBLL();
            List<Customer> customers = customerBLL.findAllCustomers();

            table2 = customerBLL.createTable(customers);
            jtable2 = new JTable(table2);
            jtable2.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                }
            });

            view2 = new View(jtable2);
            view2.attachButton7(new getData());

        }

    }

    class addProduct implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            ProductBLL productBLL = new ProductBLL();
            Product p = new Product(16, "Toshi", 2, 100);
            List<Product> list = new ArrayList<>();
            list.add(p);
            Product product2 = productBLL.insert(list);

        }
    }

    class addCustomer implements ActionListener {


        public void actionPerformed(ActionEvent e) {

            CustomerBLL customerBLL = new CustomerBLL();
            Customer customer1 = new Customer(19, "Buni");
            List<Customer> list = new ArrayList<>();
            list.add(customer1);
            Customer customer2 = customerBLL.insert(list);
            table2.fireTableRowsInserted(customer1.getId(), customer1.getId());

        }

    }

    class addOrder implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            OrderBLL orderBLL = new OrderBLL();
            ProductBLL productBLL = new ProductBLL();
            CustomerBLL customerBLL = new CustomerBLL();
            OrderItemBLL orderItemBLL = new OrderItemBLL();

            Product product = productBLL.findProductById(Integer.parseInt(getProductData));
            Customer customer = customerBLL.findClientById(Integer.parseInt(getCustomerData));

            if (product.getStoc() >= textDataStoc) {

                Orders order = new Orders(nrOrder, customer.getId());
                List<Orders> list = new ArrayList<>();
                list.add(order);
                Orders orders2 = orderBLL.insert(list);
                //table3.fireTableRowsInserted(order.getId(), order.getId());

                OrderItem orderItem = new OrderItem(nrOrderItem,order.getId(),product.getId(),textDataStoc);
                List<OrderItem> list1 = new ArrayList<>();
                list1.add(orderItem);
                OrderItem orderItem2 = orderItemBLL.insert(list1);

                nrOrder++;
                nrOrderItem++;

                productBLL.update(product,"stoc","id",product.getStoc()-textDataStoc,product.getId());
                try{
                    PrintWriter writer = new PrintWriter("Bill.txt", "UTF-8");
                   // writer.println("The first line");
                    writer.println(customer.getName() + " a plasat comanda " + order.getId() +" cumparand un " +product.getName() + " costand " +product.getPret());
                    //writer.println("The second line");
                    writer.close();
                } catch (IOException f) {
                    // do something
                    f.printStackTrace();

                }
               //System.out.println(customer.getName() + " a plasat comanda " + order.getId() +" cumparand un " +product.getName() + " costand " +product.getPret());

            }
            else{
                System.out.println(product.getStoc()-textDataStoc);
                System.out.println("UNDER STOCK!");

            }
        }
    }

    class showOrders implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            OrderBLL orderBLL = new OrderBLL();
            List<Orders> orders = orderBLL.findAllOrders();

            table3 = orderBLL.createTable(orders);
            jtable3 = new JTable(table3);
            jtable3.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                }
            });

            view3 = new View(jtable3);
            //view.attachButton5();

        }
    }

    class showOrderItems implements ActionListener{

        public void actionPerformed(ActionEvent e){

            OrderItemBLL orderItemBLL = new OrderItemBLL();
            List<OrderItem> orderItems = orderItemBLL.findAllOrderItems();

            table4 = orderItemBLL.createTable(orderItems);
            jtable4 = new JTable(table4);
            jtable4.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                }
            });

            view4 = new View(jtable4);
            //view.attachButton5();

        }
    }


    class deleteCustomer implements ActionListener

    {

        public void actionPerformed(ActionEvent e) {

            CustomerBLL customerBLL = new CustomerBLL();

            Customer customer = customerBLL.findClientById(Integer.parseInt(getCustomerData));

            System.out.println(customer.toString());

            customerBLL.delete(customer, "id", customer.getId());

        }
    }

    class deleteProduct implements ActionListener

    {

        public void actionPerformed(ActionEvent e) {

            ProductBLL productBLL = new ProductBLL();

            Product product = productBLL.findProductById(Integer.parseInt(getProductData));

           // System.out.println(p.toString());

           productBLL.delete(product, "id", product.getId());

        }
    }

    class updateCustomer implements ActionListener

    {

        public void actionPerformed(ActionEvent e) {
        }
    }

    class updateProduct implements ActionListener

    {

        public void actionPerformed(ActionEvent e) {
        }
    }


    class getData implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            int customerColumn = jtable2.getSelectedColumn();
            int customerRow = jtable2.getSelectedRow();
            getCustomerData = jtable2.getModel().getValueAt(customerRow, customerColumn).toString();
            System.out.println(getCustomerData);

        }

    }

    class getProdId implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            int productColumn = jtable1.getSelectedColumn();
            int productRow = jtable1.getSelectedRow();
            getProductData = jtable1.getModel().getValueAt(productRow, productColumn).toString();
            System.out.println(getProductData);

        }

    }

    class getProdQ implements ActionListener {
        public void actionPerformed(ActionEvent e) {

           textDataStoc = view1.getQuantity();
        }
    }
}
