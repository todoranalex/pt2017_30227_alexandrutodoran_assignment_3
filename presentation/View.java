package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class View extends JFrame {


    private JTable table;
    private JPanel mainPanel = new JPanel();
    private JPanel mainPanel1 = new JPanel();
    private JPanel mainPanel2 = new JPanel();
    private JTextField getq = new JTextField("quantity");
    private JButton button1 = new JButton("showProducts");
    private JButton button2 = new JButton("showCustomers");
    private JButton button3 = new JButton("insertProduct");
    private JButton button4 = new JButton("insertCustomer");
    private JButton button5 = new JButton("placeOrder");
    private JButton button6 = new JButton("showOrders");
    private JButton button7 = new JButton("getData");
    private JButton button8 = new JButton("deleteCustomer");
    private JButton button9 = new JButton("deleteProduct");
    private JButton button10 = new JButton("updateCustomer");
    private JButton button11 = new JButton("updateProduct");
    private JButton button12 = new JButton("getId");
    private JButton button13 = new JButton("getQuantity");
    private JButton button14 = new JButton("showOrderItems");


    private void tableSetup(JTable table) {
        this.table = table;
        setLayout(new FlowLayout());

        table.setPreferredScrollableViewportSize(new Dimension(500, 150));
        table.setFillsViewportHeight(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);

        setSize(600, 400);
        setVisible(true);


    }

    public View() {

        add(mainPanel);
        mainPanel.setLayout(null);
        addComponents();
        jFrameSetup();

    }

    private void addComponents() {

        button1.setBounds(400, 400, 150, 30);
        button2.setBounds(200, 400, 150, 30);
        button3.setBounds(400, 350, 150, 30);
        button4.setBounds(200, 350, 150, 30);
        button5.setBounds(600,350,150,30);
        button6.setBounds(600,400,150,30);
        button7.setBounds(800,350,150,30);
        button8.setBounds(200,450,150,30);
        button9.setBounds(400,450,150,30);
        button10.setBounds(200,500,150,30);
        button11.setBounds(400,500,150,30);
        button12.setBounds(800,400,150,30);
        //button13.setBounds()
        button14.setBounds(800,350,150,30);
        getq.setBounds(1000,400,150,30);

        mainPanel.add(button1);
        mainPanel.add(button2);
        mainPanel.add(button3);
        mainPanel.add(button4);
        mainPanel.add(button5);
        mainPanel.add(button6);
        mainPanel.add(button8);
        mainPanel.add(button9);
        mainPanel.add(button10);
        mainPanel.add(button11);
        mainPanel.add(button14);

    }

    public void attachButton1(ActionListener a) {
        button1.addActionListener(a);
    }

    public void attachButton2(ActionListener a) {
        button2.addActionListener(a);
    }

    public void attachButton3(ActionListener a) {
        button3.addActionListener(a);
    }

    public void attachButton4(ActionListener a) {
        button4.addActionListener(a);
    }

    public void attachButton5(ActionListener a) { button5.addActionListener(a);}

    public void attachButton6(ActionListener a) { button6.addActionListener(a);}

    public void attachButton7(ActionListener a) { button7.addActionListener(a);}

    public void attachButton8(ActionListener a) { button8.addActionListener(a);}

    public void attachButton9(ActionListener a) { button9.addActionListener(a);}

    public void attachButton10(ActionListener a) { button10.addActionListener(a);}

    public void attachButton11(ActionListener a) { button11.addActionListener(a);}

    public void attachButton12(ActionListener a) { button12.addActionListener(a);}

    public void attachButton13(ActionListener a) { button13.addActionListener(a);}

    public void attachButton14(ActionListener a) { button14.addActionListener(a);}

    public int getQuantity(){
        return Integer.parseInt(getq.getText());
    }


    public View(JTable table) {


        tableSetup(table);
        //add(mainPanel);
        //mainPanel.setLayout(null);
        //addComponents();
        jFrameSetup1();
        mainPanel1.add(button7);
        add(mainPanel1);
    }

    public View(JTable table, int ok1){
        tableSetup(table);
        //add(mainPanel);
        //mainPanel.setLayout(null);
        //addComponents();
        jFrameSetup2();
        add(mainPanel2);
        mainPanel2.add(button12);
        mainPanel2.add(button13);
        mainPanel2.add(getq);

    }

    private void jFrameSetup() {
        setSize(1000, 600);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private void jFrameSetup1() {
        setSize(900, 270);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        // setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

    }
    private void jFrameSetup2() {
        setSize(900, 270);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        // setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

    }

}

